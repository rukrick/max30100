#include "max30100.h"
#include "i2c.h"


uint8_t reade_byte_max (I2C_HandleTypeDef *hi2c, uint16_t dev_adr, uint8_t *p_data, uint16_t Size, uint32_t Timeout)
	{
		int state = HAL_I2C_Master_Receive (hi2c, DEV_RD, p_data, Size, Timeout);
		if(state == HAL_OK)
		{
			printf("read_byte_OK\n");
			return 0;
		}
		else
		{
			printf("read_byte_ERR\n");
			return 1;
		}
	}


uint8_t write_byte_max (I2C_HandleTypeDef *hi2c, uint16_t dev_adr, uint8_t *p_data, uint16_t Size, uint32_t Timeout)
	{
		int state = HAL_I2C_Master_Transmit (hi2c, dev_adr, p_data, Size, Timeout);
		if(state == HAL_OK)
		{
			printf("write_byte_OK\n");
			return 0;
		}
		else
		{
			printf("write_byte_ERR\n");
			return 1;
		}
	}
