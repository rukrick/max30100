#ifndef __MAX30100_H
#define __MAX30100_H

#include "main.h"

#define DEV_RD		((uint8_t)0XAF)
#define DEV_WR		((uint8_t)0XAE)

#define REG_STAT_INT		((uint8_t)0X00)
#define REG_STAT_EN			((uint8_t)0X01)

#define REG_FIFO_WR_PNT		((uint8_t)0X02)
#define REG_FIFO_OWFL_CNT	((uint8_t)0X03)
#define REG_FIFO_RD_PNT		((uint8_t)0X04)
#define REG_FIFO_DATA		((uint8_t)0X05)

#define REG_CONFIG_MODE		((uint8_t)0X06)
#define REG_CONFIG_SPO2		((uint8_t)0X07)
//#define REG_CONFIG_RESERV
#define REG_CONFIG_LED		((uint8_t)0X06)
//#define REG_CONFIG_RESERV

#define REG_TEMPER_INT		((uint8_t)0X16)
#define REG_TEMPER_FRACTION	((uint8_t)0X17)

//#define REG_PARTID_REV
#define REG_PARTID		((uint8_t)0XFF)

uint8_t reade_byte_max (I2C_HandleTypeDef *hi2c, uint16_t dev_adr, uint8_t *p_data, uint16_t Size, uint32_t Timeout);
uint8_t write_byte_max (I2C_HandleTypeDef *hi2c, uint16_t dev_adr, uint8_t *p_data, uint16_t Size, uint32_t Timeout);


#endif

